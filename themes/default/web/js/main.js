$(function(){

	function inInt(el){

		var $this = $(el),
			items = el.dom.$oItems,
			dot = $(el.dom.$el).find('.owl-controls .owl-dot');

		$.each(items, function(i){
			var item = $(items[i]),
				alt = item.find('img').attr('alt');

				$(dot[i]).html('<div class="dot-text dot-'+(i+1)+'">'+ alt+'</div>')					 
		})

	};	


	$('.owlcarousel').each(function(){
		var carousel = $(this);

		if(carousel.hasClass('title-pagination')){
			var owl = carousel.owlCarousel({
	 				items:1,
					loop:true,
					nav:true,
					onInitialized : function(){
 
						 inInt(this);
					}
	 			});		

		}else{
			carousel.owlCarousel({
				items:1,
				loop:true,
				nav:true
			});
		}


	});




 






	if($(window).width() > 480){
		// Parallax

		$(window).stellar({
			positionProperty: 'transform'
		});
	}

	


	var nav  = $('.main-nav'),
		stickyNavTop = $('.main-nav').offset().top;

	var stickyNav = function(){
		var scrollTop = $(window).scrollTop();
		     
		if (scrollTop > stickyNavTop) { 
		    $('body').addClass('have-fixed');
		    nav.addClass('fixed');
		} else {
			$('body').removeClass('have-fixed');
		    nav.removeClass('fixed'); 
		}
	};

	stickyNav();

	$(window).scroll(function() {
		stickyNav();
	});


 // Modal

	var Modal = (function(jQuery,window,document) {

		var modalWindow,
			modalTrigger = $('.js-modal-show'),

			getModalId = function(el){
				return modalWindow = $('#'+el.data('modal'));
			},

			showModal = function(el){
				$('html').addClass('locked');
				getModalId(el);
  				modalWindow.addClass('show');
			},

			removeModal = function(el){
				$('html').removeClass('locked');
  				modalWindow.removeClass('show');
			},

			setHeight = function(){
				return modalWindow.css('height', $(window).height() + 'px');
			},

			updateHeight = function(){

				$(window).on('resize', function(){
					setHeight();
				});

			},

			initScrollBar = function(){
				setHeight();
				updateHeight();
				modalWindow.find('.content').mCustomScrollbar();
			};

			modalTrigger.on('click', function(e){
				e.preventDefault();
				showModal($(this));
				initScrollBar();


			});

			$(document).on('click', '.close, .md-overlay', function(){
				removeModal();
			});

	})(jQuery,window,document);


	
})