<?php
/**
 * @var $this QuestionController
 * @var $form CActiveForm
 * @var $model SubscribeForm
 */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
?>

<div class="success-page subscribe-page">
    <div class="top-logo center text-center">
        <?= CHtml::image($mainAssets . '/images/mail-logo.png', 'Oнлайн-игра от Disney', array('class' => 'img-responsive')); ?>
    </div>
    <div class="cols">
        <div class="col text-center pull-right col-right">
            <div class="subscribe-page-intro">
                <div class="text">
                    <p>Укажите адрес электронной почты, на который <br>
                        будет выслан код 7-дневной подписки <br>
                        на онлайн-игру от Disney <br>
                        «Клуб пингвинов»</p>
                </div>
            </div>
            <?php
            $form = $this->beginWidget(
                'CActiveForm',
                array(
                    'id'                     => 'subscribe-form',
                    'enableClientValidation' => true,
                )
            ); ?>
            <div class="buttons">
                <div class="row">
                    <?= $form->emailField($model, 'email', array('placeholder' => $model->getAttributeLabel('email'), 'required' => true)); ?>
                </div>
                <div class="row">
                    <button type="submit" class="btn btn-4 btn-subscribe"><span>Получить код подписки</span></button>
                </div>
            </div>
            <?php $this->endWidget(); ?>

            <div class="prompt">
                Адрес будет использован для однократной <br>отправки кода и не будет храниться после этого
            </div>
        </div>
        <div class="col pull-left col-left text-center">
            <div class="paff"></div>
        </div>
    </div>
</div>