<?php
/**
 * @var $this QuestionController
 * @var $session UserSession
 */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
$outcome = $session->getOutcome();

Yii::app()->clientScript->registerMetaTag($outcome->name, null, null, ['property' => 'og:title']);
Yii::app()->clientScript->registerMetaTag($session->getUrl(true), null, null, ['property' => 'og:url']);
Yii::app()->clientScript->registerMetaTag($outcome->getImageUrl(), null, null, ['property' => 'og:image']);
Yii::app()->clientScript->registerMetaTag($outcome->text, null, null, ['property' => 'og:description']);
Yii::app()->clientScript->registerMetaTag('ru_RU', null, null, ['property' => 'og:locale']);
?>

<div class="test-body result-body">
    <div class="inner">
        <div class="result-title"><?= $outcome->name ?></div>
        <div class="result-test-image">
            <?php if ($outcome->image): ?>
                <?= CHtml::image($outcome->getImageUrl(), $outcome->name, ['class' => 'img-responsive']); ?>
            <?php endif; ?>
        </div>
        <div class="result-data">
            <div class="row">
                <div class="col-md-8">
                    <p><?= $outcome->text ?></p>
                </div>
                <div class="col-md-4 text-right">
                    <div class="share">
                        <?= CHtml::link('', 'https://www.facebook.com/sharer/sharer.php?u=' . $session->getUrl(true), ['class' => 'fb', 'target' => '_blank']); ?>
                        <?= CHtml::link('', 'https://pinterest.com/pin/create/button/?url=' . $session->getUrl(true) . '&media=' . $outcome->getImageUrl() . '&description=' . $outcome->name, ['class' => 'pin', 'target' => '_blank']); ?>
                        <?= CHtml::link('', 'https://twitter.com/intent/tweet?url=' . $session->getUrl(true) . '&text=' . $outcome->name, ['class' => 'tw', 'target' => '_blank']); ?>
                    </div>
                </div>
            </div>
        </div>
        <?= CHtml::link('Пройти тест заново', ['start'], ['class' => 'return-btn']); ?>
    </div>
</div>