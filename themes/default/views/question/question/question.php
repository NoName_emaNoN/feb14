<?php
/**
 * @var $this QuestionController
 * @var $question Question
 * @var $session UserSession
 * @var $answer UserSessionAnswer
 * @var $form CActiveForm
 */

$current = $session->answerCount + 1;
$total = Question::model()->count();
$progress = round($current / $total * 100);
?>

<div class="test-body">
    <div class="inner">
        <?php
        $form = $this->beginWidget(
            'CActiveForm',
            array(
                'id'                     => 'test-form',
                'method'                 => 'POST',
                'enableClientValidation' => true,
            )
        ); ?>
        <div class="test-title">
            <div class="num"><?= $current ?></div>
            <?= $question->text ?>
        </div>
        <div class="test-image">
            <?php if ($question->image): ?>
                <?= CHtml::image($question->getImageUrl(), '', ['class' => 'img-responsive']); ?>
            <?php endif; ?>
        </div>
        <div class="qws">

            <?= $form->hiddenField($answer, 'question_id'); ?>

            <?= $form->radioButtonList(
                $answer,
                'answer_id',
                CHtml::listData($question->answers, 'id', 'text'),
                array('template' => '<div class="item">{input}{label}</div>', 'disabled' => !$answer->isNewRecord, 'separator' => false)
            ); ?>
        </div>
        <!--        --><? //= $form->hiddenField($answer, 'answer_id'); ?>


        <div class="clearfix">
            <div class="result-num pull-right"><?= $current ?> из <?= $total ?></div>
            <div class="bar">
                <div class="progress">
                    <div style="width: <?= $progress ?>%" class="p"></div>
                </div>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>


<script>
    $('#UserSessionAnswer_answer_id').on('change', function () {
        $('#test-form').submit();
    });
</script>