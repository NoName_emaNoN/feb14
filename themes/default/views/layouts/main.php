<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="<?php echo Yii::app()->charset; ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <?php
    $mainAssets = Yii::app()->getTheme()->getAssetsUrl();

    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/module-test-section.css');

    Yii::app()->getClientScript()->registerCoreScript('jquery');
    ?>
</head>
<body>

<?= $content; ?>
<!-- flashMessages -->
<!--    --><?php //$this->widget('yupe\widgets\YFlashMessages'); ?>

<?php if (Yii::app()->hasModule('contentblock')): ?>
    <?php $this->widget(
        "application.modules.contentblock.widgets.ContentBlockWidget",
        array("code" => "STAT", "silent" => true)
    ); ?>
<?php endif; ?>

</body>
</html>