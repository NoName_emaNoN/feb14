<?php

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
?>
<div class="screen_1 animated zoomIn">
    <div class="main-page-logo text-center">
        <?= CHtml::image($mainAssets . '/images/mail-logo.png', 'Oнлайн-игра от Disney', array('class' => 'img-responsive')); ?>
    </div>
    <div class="intro-text-1 text-center">Oнлайн-игра от Disney</div>
    <div class="intro-text-2 text-center">Представляет</div>
    <div class="b-link text-center">
        <a href="http://www.disney.ru/pingvin">http://www.disney.ru/pingvin</a>
    </div>
</div>

<div class="screen_2 text-center animated zoomIn">
    <div class="intro-text">
        Тест <br>Безопасность в сети Интернет
    </div>
    <div class="center-image">
        <?= CHtml::image($mainAssets . '/images/screen_1_2.png', '', array('class' => 'img-responsive')); ?>
    </div>
    <?= CHtml::link('<span>Пройти тест</span>', array('/question/question/start'), array('class' => 'btn btn-1')); ?>
</div>