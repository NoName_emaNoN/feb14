<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('usersession')->getCategory() => array(),
        Yii::t('usersession', 'Ответы пользователей') => array('/usersession/usersessionBackend/index'),
        Yii::t('usersession', 'Добавление'),
    );

    $this->pageTitle = Yii::t('usersession', 'Ответы пользователей - добавление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('usersession', 'Управление ответами пользователей'), 'url' => array('/usersession/usersessionBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('usersession', 'Добавить ответ пользователя'), 'url' => array('/usersession/usersessionBackend/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('usersession', 'Ответы пользователей'); ?>
        <small><?php echo Yii::t('usersession', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>