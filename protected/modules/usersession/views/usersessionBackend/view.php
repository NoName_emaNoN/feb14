<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('usersession')->getCategory() => array(),
        Yii::t('usersession', 'Ответы пользователей') => array('/usersession/usersessionBackend/index'),
        $model->id,
    );

    $this->pageTitle = Yii::t('usersession', 'Ответы пользователей - просмотр');

    $this->menu = array(
        array('icon' => 'list-alt', 'label' => Yii::t('usersession', 'Управление ответами пользователей'), 'url' => array('/usersession/usersessionBackend/index')),
        array('icon' => 'plus-sign', 'label' => Yii::t('usersession', 'Добавить ответ пользователя'), 'url' => array('/usersession/usersessionBackend/create')),
        array('label' => Yii::t('usersession', 'Ответ пользователя') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'pencil', 'label' => Yii::t('usersession', 'Редактирование ответа пользователя'), 'url' => array(
            '/usersession/usersessionBackend/update',
            'id' => $model->id
        )),
        array('icon' => 'eye-open', 'label' => Yii::t('usersession', 'Просмотреть ответ пользователя'), 'url' => array(
            '/usersession/usersessionBackend/view',
            'id' => $model->id
        )),
        array('icon' => 'trash', 'label' => Yii::t('usersession', 'Удалить ответ пользователя'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/usersession/usersessionBackend/delete', 'id' => $model->id),
            'confirm' => Yii::t('usersession', 'Вы уверены, что хотите удалить ответ пользователя?'),
            'params' => array(Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken),
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('usersession', 'Просмотр') . ' ' . Yii::t('usersession', 'ответа пользователя'); ?>        <br/>
        <small>&laquo;<?php echo $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
'data'       => $model,
'attributes' => array(
        'id',
        'user_id',
        'closed',
        'close_date',
),
)); ?>
