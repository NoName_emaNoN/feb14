<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = array(
    Yii::app()->getModule('usersession')->getCategory() => array(),
    Yii::t('usersession', 'Ответы пользователей')       => array('/usersession/usersessionBackend/index'),
    Yii::t('usersession', 'Управление'),
);

$this->pageTitle = Yii::t('usersession', 'Ответы пользователей - управление');

$this->menu = array(
    array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('usersession', 'Управление ответами пользователей'), 'url' => array('/usersession/usersessionBackend/index')),
    array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('usersession', 'Добавить ответ пользователя'), 'url' => array('/usersession/usersessionBackend/create')),
);
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('usersession', 'Ответы пользователей'); ?>
        <small><?php echo Yii::t('usersession', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?php echo Yii::t('usersession', 'Поиск ответов пользователей'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
    Yii::app()->clientScript->registerScript(
        'search',
        "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('user-session-grid', {
            data: $(this).serialize()
        });

        return false;
    });
"
    );
    $this->renderPartial('_search', array('model' => $model));
    ?>
</div>

<br/>

<p> <?php echo Yii::t('usersession', 'В данном разделе представлены средства управления ответами пользователей'); ?>
</p>

<?php
$this->widget(
    'yupe\widgets\CustomGridView',
    array(
        'id'           => 'user-session-grid',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => array(
            'id',
//        'user_id',
            array(
                'name'  => 'closed',
                'value' => '$data->getClosed()',
            ),
            array(
                'header' => 'Кол-во правильных ответов',
                'value'  => '$data->getRightAnswerCount() . "/" . $data->answerCount',
            ),
            'close_date',
            array(
                'class' => 'yupe\widgets\CustomButtonColumn',
            ),
        ),
    )
); ?>
