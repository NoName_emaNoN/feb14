<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('usersession')->getCategory() => array(),
        Yii::t('usersession', 'Ответы пользователей') => array('/usersession/usersessionBackend/index'),
        $model->id => array('/usersession/usersessionBackend/view', 'id' => $model->id),
        Yii::t('usersession', 'Редактирование'),
    );

    $this->pageTitle = Yii::t('usersession', 'Ответы пользователей - редактирование');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('usersession', 'Управление ответами пользователей'), 'url' => array('/usersession/usersessionBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('usersession', 'Добавить ответ пользователя'), 'url' => array('/usersession/usersessionBackend/create')),
        array('label' => Yii::t('usersession', 'Ответ пользователя') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('usersession', 'Редактирование ответа пользователя'), 'url' => array(
            '/usersession/usersessionBackend/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('usersession', 'Просмотреть ответ пользователя'), 'url' => array(
            '/usersession/usersessionBackend/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('usersession', 'Удалить ответ пользователя'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/usersession/usersessionBackend/delete', 'id' => $model->id),
            'confirm' => Yii::t('usersession', 'Вы уверены, что хотите удалить ответ пользователя?'),
            'params' => array(Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken),
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('usersession', 'Редактирование') . ' ' . Yii::t('usersession', 'ответа пользователя'); ?>        <br/>
        <small>&laquo;<?php echo $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>