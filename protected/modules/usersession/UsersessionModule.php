<?php

use yupe\components\WebModule;

class UsersessionModule extends WebModule
{
    const VERSION = '0.9';

    public function getDependencies()
    {
        return array('question');
    }

    public function getVersion()
    {
        return self::VERSION;
    }

    public function getCategory()
    {
        return Yii::t('UsersessionModule.Useression', 'Вопросы');
    }

    public function getName()
    {
        return Yii::t('UsersessionModule.Useression', 'Ответы пользователей');
    }

    public function getDescription()
    {
        return Yii::t('UsersessionModule.Useression', 'Ответы пользователей');
    }

    public function getAuthor()
    {
        return Yii::t('UsersessionModule.Useression', 'yupe team');
    }

    public function getAuthorEmail()
    {
        return Yii::t('UsersessionModule.Useression', 'team@yupe.ru');
    }

    public function getUrl()
    {
        return Yii::t('UsersessionModule.Useression', 'http://zexed.net');
    }

    public function getIcon()
    {
        return 'fa fa-fw fa-tasks';
    }

    public function getAdminPageLink()
    {
        return '/usersession/usersessionBackend/index';
    }

    public function init()
    {
        $this->setImport(
            array(
                'application.modules.usersession.models.*',
                'application.modules.usersession.components.*'
            )
        );

        parent::init();
    }

    public function getNavigation()
    {
        return array(
            array(
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('UsersessionModule.Useression', 'Список ответов'),
                'url'   => array('/usersession/usersessionBackend/index')
            ),
        );
    }

    public function getAuthItems()
    {
        return array(
            array(
                'name'        => 'UserSession.UsersessionManager',
                'description' => Yii::t('UsersessionModule.Useression', 'Manage Useression'),
                'type'        => AuthItem::TYPE_TASK,
                'items'       => array(
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'UserSession.UserSessionBackend.Create',
                        'description' => Yii::t('UsersessionModule.Useression', 'Creating Useression')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'UserSession.UserSessionBackend.Delete',
                        'description' => Yii::t('UsersessionModule.Useression', 'Removing Useression')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'UserSession.UserSessionBackend.Index',
                        'description' => Yii::t('UsersessionModule.Useression', 'List of Useression')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'UserSession.UserSessionBackend.Update',
                        'description' => Yii::t('UsersessionModule.Useression', 'Editing Useression')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'UserSession.UserSessionBackend.Inline',
                        'description' => Yii::t('UsersessionModule.Useression', 'Editing Useression')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'UserSession.UserSessionBackend.View',
                        'description' => Yii::t('UsersessionModule.Useression', 'Viewing Useression')
                    ),
                )
            )
        );
    }
}
