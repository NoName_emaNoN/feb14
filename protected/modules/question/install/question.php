<?php
return array(
    'module'    => array(
        'class' => 'application.modules.question.QuestionModule',
    ),
    'import'    => array(
        'application.modules.question.models.*',
        'application.modules.queue.models.*',
        'application.modules.queue.*',
    ),
    'component' => array(),
    'rules'     => array(
        '/start'                => '/question/question/start',
        '/question'             => '/question/question/question',
        '/check'                => '/question/question/check',
        '/reset'                => '/question/question/reset',
        '/result/<session:\d+>' => '/question/question/result',
    ),
);