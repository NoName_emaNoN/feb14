<?php

class m130814_200823_create_user_session_table extends \yupe\components\DbMigration
{
    public function up()
    {
        $this->createTable(
            '{{question_user_session}}',
            array(
                'id'         => 'pk',
                'user_id'    => 'string NOT NULL',
                'status'     => 'tinyint(1) NOT NULL DEFAULT 0',
                'close_date' => 'DATETIME',
            ),
            $this->getOptions()
        );

        $this->createIndex('ix_{{question_user_session}}_status', '{{question_user_session}}', 'status');
    }

    public function down()
    {
        $this->dropTable('{{question_user_session}}');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}