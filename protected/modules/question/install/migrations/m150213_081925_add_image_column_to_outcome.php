<?php

class m150213_081925_add_image_column_to_outcome extends CDbMigration
{
    public function up()
    {
        $this->addColumn('{{question_outcome}}', 'image', 'string DEFAULT NULL');
    }

    public function down()
    {
        $this->dropColumn('{{question_outcome}}', 'image');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}