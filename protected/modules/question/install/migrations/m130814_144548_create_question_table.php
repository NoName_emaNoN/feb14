<?php

class m130814_144548_create_question_table extends \yupe\components\DbMigration
{
    public function up()
    {
        $this->createTable(
            '{{question_question}}',
            array(
                'id'     => 'pk',
                'text'   => 'text',
                'status' => 'tinyint(1) NOT NULL DEFAULT 1',
            ),
            $this->getOptions()
        );

        $this->createTable(
            '{{question_answer}}',
            [
                'id'             => 'pk',
                'question_id'    => 'integer NOT NULL',
                'text'           => 'string NOT NULL',
                'outcome_id'     => 'integer NOT NULL',
                'outcome_weight' => 'tinyint(3) NOT NULL DEFAULT 1',
            ],
            $this->getOptions()
        );

        $this->createTable(
            '{{question_outcome}}',
            [
                'id'        => 'pk',
                'name'      => 'string NOT NULL',
                'text'      => 'text',
                'css_class' => 'string NOT NULL',
            ],
            $this->getOptions()
        );

        $this->createIndex('ix_{{question_question}}_status', '{{question_question}}', 'status');
        $this->createIndex('ix_{{question_answer}}_question_id', '{{question_answer}}', 'question_id');

        $this->addForeignKey('fk_{{question_answer}}_question_id', '{{question_answer}}', 'question_id', '{{question_question}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_{{question_answer}}_outcome_id', '{{question_answer}}', 'outcome_id', '{{question_outcome}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTableWithForeignKeys('{{question_answer}}');
        $this->dropTableWithForeignKeys('{{question_outcome}}');
        $this->dropTableWithForeignKeys('{{question_question}}');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}