<?php

class m150213_025936_add_image_column_to_question extends CDbMigration
{
    public function up()
    {
        $this->addColumn('{{question_question}}', 'image', 'string DEFAULT NULL');
    }

    public function down()
    {
        $this->dropColumn('{{question_question}}', 'image');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}