<?php

class m130814_202722_create_user_session_question_table extends \yupe\components\DbMigration
{
    public function up()
    {
        $this->createTable(
            '{{question_user_session_answer}}',
            array(
                'id'          => 'pk',
                'session_id'  => 'integer NOT NULL',
                'question_id' => 'integer NOT NULL',
                'answer_id'   => 'integer NOT NULL',
            ),
            $this->getOptions()
        );

        $this->addForeignKey('fk_session_id_id', '{{question_user_session_answer}}', 'session_id', '{{question_user_session}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_question_id_id', '{{question_user_session_answer}}', 'question_id', '{{question_question}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_answer_id_id', '{{question_user_session_answer}}', 'answer_id', '{{question_answer}}', 'id', 'CASCADE');

        $this->createIndex('session_question', '{{question_user_session_answer}}', 'session_id, question_id', true);
    }

    public function down()
    {
        $this->dropTableWithForeignKeys('{{question_user_session_answer}}');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}