<?php

/**
 * This is the model class for table "{{question_answer}}".
 *
 * The followings are the available columns in table '{{question_answer}}':
 * @property integer $id
 * @property integer $question_id
 * @property string $text
 * @property integer $outcome_id
 * @property integer $outcome_weight
 *
 * The followings are the available model relations:
 * @property QuestionOutcome $outcome
 * @property Question $question
 */
class QuestionAnswer extends yupe\models\YModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{question_answer}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('question_id, text, outcome_id', 'required'),
            array('question_id, outcome_id, outcome_weight', 'numerical', 'integerOnly' => true),
            array('text', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, question_id, text, outcome_id, outcome_weight', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'outcome'  => array(self::BELONGS_TO, 'QuestionOutcome', 'outcome_id'),
            'question' => array(self::BELONGS_TO, 'Question', 'question_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'             => 'ID',
            'question_id'    => 'Вопрос',
            'text'           => 'Текст',
            'outcome_id'     => 'Исход',
            'outcome_weight' => 'Вес исхода',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.question_id', $this->question_id);
        $criteria->compare($this->tableAlias . '.text', $this->text, true);
        $criteria->compare($this->tableAlias . '.outcome_id', $this->outcome_id);
        $criteria->compare($this->tableAlias . '.outcome_weight', $this->outcome_weight);

        return new CActiveDataProvider(
            $this, array(
                'criteria' => $criteria,
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return QuestionAnswer the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
