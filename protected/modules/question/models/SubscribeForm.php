<?php

/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 18.11.2014
 * Time: 22:16
 */
class SubscribeForm extends \yupe\models\YFormModel
{
    public $email;

    public function rules()
    {
        return array(
            array('email', 'required'),
            array('email', 'email'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'email' => 'E-mail',
        );
    }
}