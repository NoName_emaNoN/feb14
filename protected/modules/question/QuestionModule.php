<?php

use yupe\components\WebModule;

class QuestionModule extends WebModule
{
    const VERSION = '0.9';

    public $uploadPath = 'question';
    public $allowedExtensions = 'jpg,jpeg,png,gif';
    public $minSize = 0;
    public $maxSize = 5368709120;
    public $maxFiles = 1;

    public function getDependencies()
    {
        return array('queue');
    }

    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir(Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath, 0755);
        }

        return false;
    }

    public function checkSelf()
    {
        $messages = [];

        $uploadPath = Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath;

        if (!is_writable($uploadPath)) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type'    => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    'QuestionModule.question',
                    'Directory "{dir}" is not accessible for write! {link}',
                    [
                        '{dir}'  => $uploadPath,
                        '{link}' => CHtml::link(
                            Yii::t('QuestionModule.question', 'Change settings'),
                            [
                                '/yupe/backend/modulesettings/',
                                'module' => 'question',
                            ]
                        ),
                    ]
                ),
            ];
        }

        return (isset($messages[WebModule::CHECK_ERROR])) ? $messages : true;
    }

    public function getEditableParams()
    {
        return array(
            'adminMenuOrder',
            'uploadPath',
            'editor' => Yii::app()->getModule('yupe')->getEditors(),
            'uploadPath',
            'allowedExtensions',
            'minSize',
            'maxSize',
        );
    }

    public function getParamsLabels()
    {
        return array(
            'adminMenuOrder'    => Yii::t('QuestionModule.question', 'Menu items order'),
            'editor'            => Yii::t('QuestionModule.question', 'Visual Editor'),
            'uploadPath'        => Yii::t(
                'QuestionModule.question',
                'Uploading files catalog (relatively {path})',
                [
                    '{path}' => Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . Yii::app()->getModule(
                            "yupe"
                        )->uploadPath
                ]
            ),
            'allowedExtensions' => Yii::t('QuestionModule.question', 'Accepted extensions (separated by comma)'),
            'minSize'           => Yii::t('QuestionModule.question', 'Minimum size (in bytes)'),
            'maxSize'           => Yii::t('QuestionModule.question', 'Maximum size (in bytes)'),
        );
    }

    public function getIsInstallDefault()
    {
        return true;
    }

    public function getVersion()
    {
        return self::VERSION;
    }

    public function getCategory()
    {
        return Yii::t('QuestionModule.question', 'Контент');
    }

    public function getName()
    {
        return Yii::t('QuestionModule.question', 'Вопросы');
    }

    public function getDescription()
    {
        return Yii::t('QuestionModule.question', 'Модуль для создания и управления опросами');
    }

    public function getAuthor()
    {
        return Yii::t('QuestionModule.question', 'Mikhail Chemezov');
    }

    public function getAuthorEmail()
    {
        return Yii::t('QuestionModule.question', 'michlenanosoft@gmail.com');
    }

    public function getUrl()
    {
        return Yii::t('QuestionModule.question', 'http://zexed.net');
    }

    public function getIcon()
    {
        return 'fa fa-fw fa-file';
    }

    public function isMultiLang()
    {
        return false;
    }

    public function init()
    {
        parent::init();

        $this->setImport(
            array(
                'question.models.*',
                'question.components.*',
            )
        );
    }

    public function getNavigation()
    {
        return array(
            ['label' => 'Вопросы'],
            array(
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('QuestionModule.question', 'Список вопросов'),
                'url'   => array('/question/questionBackend/index')
            ),
            array(
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('QuestionModule.question', 'Добавить вопрос'),
                'url'   => array('/question/questionBackend/create')
            ),
            ['label' => 'Ответы'],
            array(
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('QuestionModule.question', 'Список ответов'),
                'url'   => array('/question/questionAnswerBackend/index')
            ),
            array(
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('QuestionModule.question', 'Добавить ответ'),
                'url'   => array('/question/questionAnswerBackend/create')
            ),
            ['label' => 'Исходы'],
            array(
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('QuestionModule.question', 'Список исходов'),
                'url'   => array('/question/questionOutcomeBackend/index')
            ),
            array(
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('QuestionModule.question', 'Добавить исход'),
                'url'   => array('/question/questionOutcomeBackend/create')
            ),
        );
    }

    public function getAdminPageLink()
    {
        return '/question/questionBackend/index';
    }

    public function getAuthItems()
    {
        return array(
            array(
                'name'        => 'Question.QuestionManager',
                'description' => Yii::t('QuestionModule.question', 'Manage categories'),
                'type'        => AuthItem::TYPE_TASK,
                'items'       => array(
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Question.QuestionBackend.Create',
                        'description' => Yii::t('QuestionModule.question', 'Creating question')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Question.QuestionBackend.Delete',
                        'description' => Yii::t('QuestionModule.question', 'Removing question')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Question.QuestionBackend.Index',
                        'description' => Yii::t('QuestionModule.question', 'List of categories')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Question.QuestionBackend.Update',
                        'description' => Yii::t('QuestionModule.question', 'Editing categories')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Question.QuestionBackend.Inline',
                        'description' => Yii::t('QuestionModule.question', 'Editing categories')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Question.QuestionBackend.View',
                        'description' => Yii::t('QuestionModule.question', 'Viewing categories')
                    ),
                )
            )
        );
    }
}
