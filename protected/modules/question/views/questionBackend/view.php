<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('question')->getCategory() => array(),
        Yii::t('question', 'Вопросы') => array('/question/questionBackend/index'),
        $model->id,
    );

    $this->pageTitle = Yii::t('question', 'Вопросы - просмотр');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('question', 'Управление вопросами'), 'url' => array('/question/questionBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('question', 'Добавить вопрос'), 'url' => array('/question/questionBackend/create')),
        array('label' => Yii::t('question', 'Вопрос') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('question', 'Редактирование вопроса'), 'url' => array(
            '/question/questionBackend/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('question', 'Просмотреть вопрос'), 'url' => array(
            '/question/questionBackend/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('question', 'Удалить вопрос'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/question/questionBackend/delete', 'id' => $model->id),
            'confirm' => Yii::t('question', 'Вы уверены, что хотите удалить вопрос?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('question', 'Просмотр') . ' ' . Yii::t('question', 'вопроса'); ?>        <br/>
        <small>&laquo;<?php echo $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
'data'       => $model,
'attributes' => array(
        'id',
        'text',
        'status',
),
)); ?>
