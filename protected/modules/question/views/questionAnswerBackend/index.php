<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = array(
    Yii::app()->getModule('question')->getCategory() => array(),
    Yii::t('question', 'Ответы')                     => array('/question/questionAnswerBackend/index'),
    Yii::t('question', 'Управление'),
);

$this->pageTitle = Yii::t('question', 'Ответы - управление');

$this->menu = array(
    array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('question', 'Управление ответами'), 'url' => array('/question/questionAnswerBackend/index')),
    array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('question', 'Добавить ответ'), 'url' => array('/question/questionAnswerBackend/create')),
);
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('question', 'Ответы'); ?>
        <small><?php echo Yii::t('question', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?php echo Yii::t('question', 'Поиск ответов'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
    Yii::app()->clientScript->registerScript(
        'search',
        "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('question-answer-grid', {
            data: $(this).serialize()
        });

        return false;
    });
"
    );
    $this->renderPartial('_search', array('model' => $model));
    ?>
</div>

<br/>

<p> <?php echo Yii::t('question', 'В данном разделе представлены средства управления ответами'); ?>
</p>

<?php
$this->widget(
    'yupe\widgets\CustomGridView',
    array(
        'id'           => 'question-answer-grid',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => array(
            [
                'name'        => 'id',
                'htmlOptions' => ['width' => '60'],
            ],
            array(
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'editable' => array(
                    'url'    => $this->createUrl('inline'),
                    'type'   => 'select',
                    'title'  => 'Выберите вопрос',
                    'source' => CHtml::listData(Question::model()->findAll(), 'id', 'text'),
                    'params' => array(
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    )
                ),
                'name'     => 'question_id',
                'type'     => 'raw',
                'value'    => '$data->question->text',
                'filter'   => CHtml::activeDropDownList(
                    $model,
                    'question_id',
                    CHtml::listData(Question::model()->findAll(), 'id', 'text'),
                    array('class' => 'form-control', 'empty' => '')
                ),
            ),
            'text',
            array(
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'editable' => array(
                    'url'    => $this->createUrl('inline'),
                    'type'   => 'select',
                    'title'  => 'Выберите исход',
                    'source' => CHtml::listData(QuestionOutcome::model()->findAll(), 'id', 'name'),
                    'params' => array(
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    )
                ),
                'name'     => 'outcome_id',
                'type'     => 'raw',
                'value'    => '$data->outcome->name',
                'filter'   => CHtml::activeDropDownList(
                    $model,
                    'outcome_id',
                    CHtml::listData(QuestionOutcome::model()->findAll(), 'id', 'name'),
                    array('class' => 'form-control', 'empty' => '')
                ),
            ),
            'outcome_weight',
            array(
                'class' => 'yupe\widgets\CustomButtonColumn',
            ),
        ),
    )
); ?>
