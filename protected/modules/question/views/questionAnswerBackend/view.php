<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('question')->getCategory() => array(),
        Yii::t('question', 'Ответы') => array('/question/questionAnswerBackend/index'),
        $model->id,
    );

    $this->pageTitle = Yii::t('question', 'Ответы - просмотр');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('question', 'Управление ответами'), 'url' => array('/question/questionAnswerBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('question', 'Добавить ответ'), 'url' => array('/question/questionAnswerBackend/create')),
        array('label' => Yii::t('question', 'Ответ') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('question', 'Редактирование ответа'), 'url' => array(
            '/question/questionAnswerBackend/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('question', 'Просмотреть ответ'), 'url' => array(
            '/question/questionAnswerBackend/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('question', 'Удалить ответ'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/question/questionAnswerBackend/delete', 'id' => $model->id),
            'confirm' => Yii::t('question', 'Вы уверены, что хотите удалить ответ?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('question', 'Просмотр') . ' ' . Yii::t('question', 'ответа'); ?>        <br/>
        <small>&laquo;<?php echo $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
'data'       => $model,
'attributes' => array(
        'id',
        'question_id',
        'text',
        'outcome_id',
        'outcome_weight',
),
)); ?>
