<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('question')->getCategory() => array(),
        Yii::t('question', 'Ответы') => array('/question/questionAnswerBackend/index'),
        Yii::t('question', 'Добавление'),
    );

    $this->pageTitle = Yii::t('question', 'Ответы - добавление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('question', 'Управление ответами'), 'url' => array('/question/questionAnswerBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('question', 'Добавить ответ'), 'url' => array('/question/questionAnswerBackend/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('question', 'Ответы'); ?>
        <small><?php echo Yii::t('question', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>