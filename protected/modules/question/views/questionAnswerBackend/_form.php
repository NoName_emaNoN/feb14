<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model QuestionAnswer
 * @var $form TbActiveForm
 * @var $this QuestionAnswerBackendController
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    array(
        'id'                     => 'question-answer-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'htmlOptions'            => array('class' => 'well'),
    )
);
?>

    <div class="alert alert-info">
        <?php echo Yii::t('question', 'Поля, отмеченные'); ?>
        <span class="required">*</span>
        <?php echo Yii::t('question', 'обязательны.'); ?>
    </div>

<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->dropDownListGroup($model, 'question_id', array('widgetOptions' => array('data' => CHtml::listData(Question::model()->findAll(), 'id', 'text')))); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textFieldGroup(
                $model,
                'text',
                array('widgetOptions' => array('htmlOptions' => array('class' => 'popover-help', 'data-original-title' => $model->getAttributeLabel('text'), 'data-content' => $model->getAttributeDescription('text'))))
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->dropDownListGroup($model, 'outcome_id', array('widgetOptions' => array('data' => CHtml::listData(QuestionOutcome::model()->findAll(), 'id', 'name')))); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textFieldGroup(
                $model,
                'outcome_weight',
                array('widgetOptions' => array('htmlOptions' => array('class' => 'popover-help', 'data-original-title' => $model->getAttributeLabel('outcome_weight'), 'data-content' => $model->getAttributeDescription('outcome_weight'))))
            ); ?>
        </div>
    </div>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    array(
        'buttonType' => 'submit',
        'context'    => 'primary',
        'label'      => Yii::t('question', 'Сохранить ответ и продолжить'),
    )
); ?>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    array(
        'buttonType'  => 'submit',
        'htmlOptions' => array('name' => 'submit-type', 'value' => 'index'),
        'label'       => Yii::t('question', 'Сохранить ответ и закрыть'),
    )
); ?>

<?php $this->endWidget(); ?>