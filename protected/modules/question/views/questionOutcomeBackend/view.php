<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('question')->getCategory() => array(),
        Yii::t('question', 'Исходы') => array('/question/questionOutcomeBackend/index'),
        $model->name,
    );

    $this->pageTitle = Yii::t('question', 'Исходы - просмотр');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('question', 'Управление исходами'), 'url' => array('/question/questionOutcomeBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('question', 'Добавить исход'), 'url' => array('/question/questionOutcomeBackend/create')),
        array('label' => Yii::t('question', 'Исход') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('question', 'Редактирование исхода'), 'url' => array(
            '/question/questionOutcomeBackend/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('question', 'Просмотреть исход'), 'url' => array(
            '/question/questionOutcomeBackend/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('question', 'Удалить исход'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/question/questionOutcomeBackend/delete', 'id' => $model->id),
            'confirm' => Yii::t('question', 'Вы уверены, что хотите удалить исход?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('question', 'Просмотр') . ' ' . Yii::t('question', 'исхода'); ?>        <br/>
        <small>&laquo;<?php echo $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
'data'       => $model,
'attributes' => array(
        'id',
        'name',
        'text',
        'css_class',
),
)); ?>
