<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('question')->getCategory() => array(),
        Yii::t('question', 'Исходы') => array('/question/questionOutcomeBackend/index'),
        Yii::t('question', 'Добавление'),
    );

    $this->pageTitle = Yii::t('question', 'Исходы - добавление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('question', 'Управление исходами'), 'url' => array('/question/questionOutcomeBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('question', 'Добавить исход'), 'url' => array('/question/questionOutcomeBackend/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('question', 'Исходы'); ?>
        <small><?php echo Yii::t('question', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>